//
//  OrdersTrackingModel.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/16/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation

struct noOfOrders {
    var odrId : String
}

struct ordersPassingValue {
    var itemId: String
    var itemName: String
    var itemQuantity: String
    var finalValue: String
    var mobile: String
}

struct orderhistorys{
    var orderIdRef: String
    var itemNames: String
    var quantity: String
    var price: String
    var statusOfOrder: String
}
 struct orderHistoryPassingValue{
    var mobile: String
}

struct orderTrackPassingValue {
    var orderId: String
}
struct orderTrackingValue {
    var orderStatus: String
}
