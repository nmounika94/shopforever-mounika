//
//  RegisterPageViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/14/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import TWMessageBarManager

class RegisterPageViewController: UIViewController {

    @IBOutlet weak var passwrdTF: UITextField!
   
    @IBOutlet weak var cellNo: UITextField!
    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var emailId: UITextField!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //        let str = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/cust_product.php?Id=207"
        //        let urls = URL(string: str)
        //        OrdersParsng.eachProductValue(url: urls!, eachOrderIdObj: eachpdtPassingList(idValue: "207"))
        // Do any additional setup after loading the view.
        
//        let str = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/orders.php?&item_id=407&item_names=laptop&item_quantity=1&final_price=100000&mobile=123456"
//        let urls = URL(string: str)
//        OrdersRelatedParse.noOfOrdersList(userOrdersObj: ordersPassingValue(itemId: "207", itemName: "Fashion-Set", itemQuantity: "1", finalValue: "2000", mobile: "8501929790"))
        
//        let str = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/order_history.php?&mobile=8501929790"
//        let urls = URL(string: str)
//        OrdersRelatedParse.OrderHistoryList(orderHistoryObj: orderHistoryPassingValue(mobile: "8501929790"))
//        
//        let str = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/order_track.php?order_id=2144"
//        let urls = URL(string: str)
//        OrdersRelatedParse.orderTrackingList(orderTrackPassObj: orderTrackPassingValue(orderId: "2186"))
    }
    @IBAction func signInBtn(_ sender: UIButton) {
        var pswd = passwrdTF.text
        var phneNo = cellNo.text
        
        if ((phneNo?.characters.count)! < 10 && (pswd?.characters.count)! < 8) {
            
            TWMessageBarManager.sharedInstance().showMessage(withTitle: "Invalid", description: "Please Give a number greater than 8 characters", type: .info)
           
        }
        else if (phneNo == nil && pswd == nil) {
            
            TWMessageBarManager.sharedInstance().showMessage(withTitle: "Error", description: "Please fill all the fields", type: .info)
            
        }
        TWMessageBarManager.sharedInstance().showMessage(withTitle: "Signed Up successfully", description: "You Have singed Up successfully", type: .success)
        
        parsingData.SignUpNewUse(userSignUpObj: userSignedUpModel(userName: userName.text!, userEmail: emailId.text!, userMobile: cellNo.text!  , password:passwrdTF.text!)) { (val) in
            DispatchQueue.main.async {
               let myText = val as? String
            }
           
        }
    }


    @IBAction func loginBtn(_ sender: UIButton) {
        
    
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageViewController") {
            self.present(controller, animated: true, completion: nil)
            
           
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
