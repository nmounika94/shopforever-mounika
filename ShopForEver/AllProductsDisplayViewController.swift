//
//  AllProductsDisplayViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/17/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import SDWebImage
import TWMessageBarManager

protocol CartAdding : class{
    func refreshCart()
}

class AllProductsDisplayViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var productIdArray = [String]()
    var strType: String?
    var refresh = UIRefreshControl()
    var resultsOfItems = [eachProductslist]()
    
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Products"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        refresh.isEnabled = true
        refresh.addTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
        products()
        
        
        
           }
    
    
    func refreshAction(_ sender: Any){
       products()
    }
    
    
    func products() {
        OrdersParsng.eachProductValue(id: strType!) { (arr) in
            DispatchQueue.main.async {
                self.refresh.endRefreshing()
                self.resultsOfItems = (arr as? [eachProductslist])!
                self.tblView.reloadData()
            }
        }
    }

    
    func getProductIdFromCart(){
        
        for item in globalCart{
            productIdArray.append(item.idRefere!)
        }
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tblView.dequeueReusableCell(withIdentifier: "ProductsTableViewCell", for: indexPath) as? ProductsTableViewCell
            
            let resultObj = resultsOfItems[indexPath.row]
            let priceString = resultObj.prize
            priceString.replacingOccurrences(of: ",", with: "")
            priceString.replacingOccurrences(of: "$", with: "")
            let priceInDollas = Double(priceString)!/67.0
            cell?.priceLabel.text = "Price: \(Int(priceInDollas)) $"
            
            
            cell?.descrpLabel.text = resultsOfItems[indexPath.row].descrip
            cell?.idLbl.text = resultsOfItems[indexPath.row].idRefere
            cell?.nameLbl.text = resultsOfItems[indexPath.row].prodName
            cell?.imgview.sd_setImage(with: URL(string: resultsOfItems[indexPath.row].img!), placeholderImage: UIImage(named:"noImage"))
            cell?.addToCart.tag = indexPath.row
            cell?.addToCart.addTarget(self, action: #selector(addToCartAction), for: .touchUpInside)
           
            cell?.qntyLabel.text = resultsOfItems[indexPath.row].quantity
            
          
            return cell!
       
        
    }

    
    
    func addToCartAction(sender: UIButton){
        
        getProductIdFromCart()
        let productObj = resultsOfItems[sender.tag]
        print(productObj.idRefere)
        let idvalue = productObj.idRefere
        print(idvalue)
        if (productIdArray.contains(idvalue!)){
TWMessageBarManager.sharedInstance().showMessage(withTitle: "Product  already added successfully to your cart", description: "", type: .success)
            
        }
        else{
            
            TWMessageBarManager.sharedInstance().showMessage(withTitle: "Product added successfully to your cart", description: "product added successfully", type: .success)
         
            globalCart.append(productObj)
        }
        UIView.animateKeyframes(withDuration: 4, delay: 0, options: .calculationModeCubic, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
                
            })
        }, completion: nil)
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    if let controller = storyBoard.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController {
        navigationController?.pushViewController(controller, animated: true)
    }
    }
    @IBAction func addToCart(_ sender: UIButton) {
        
            }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return resultsOfItems.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let storyBoad = UIStoryboard(name: "Main", bundle: nil)
//        if let controller = storyBoad.instantiateViewController(withIdentifier: "EachPoductViewController") as? EachPoductViewController{
//            
//            //let obj = resultsOfItems[indexPath.row]
//           // print(obj)
//            controller.strId = resultsOfItems[indexPath.row]
//           //navigationController?.pushViewController(controller, animated: true)
//            
//        }
    }
    
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
