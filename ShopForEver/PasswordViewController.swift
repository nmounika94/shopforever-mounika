//
//  PasswordViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/14/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var CnfrmPswd: UITextField!
    @IBOutlet weak var newpswd: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Reset Password"
        // Do any additional setup after loading the view.
    }
    @IBAction func SubmitBtn(_ sender: UIButton) {
        parsingData.resetPswd(resetpswdObj: ResetpasswordModel(userMobileNo: userName.text! , userpaswd: CnfrmPswd.text!, newpswd: CnfrmPswd.text!)) { (val) in
            let myText = val as? String
        }
       // parsingData.resetPswd(resetpswdObj: ResetpasswordModel(userMobileNo: userName.text! , userpaswd: CnfrmPswd.text!, newpswd: newpswd.text!))
    }

    @IBAction func loginBtn(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard?.instantiateViewController(withIdentifier: "LoginPageViewController")
       present(controller!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
