//
//  OrderHistoryViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/20/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import TWMessageBarManager

class OrderHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    
    var previousOrderHistory = [orderhistorys]()
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        tblView.rowHeight = 123
        animateTable()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Order Histories"
        
        productsHistory()
        // Do any additional setup after loading the view.
    }
    
    func animateTable() {
        tblView.reloadData()
        
        let cells = tblView.visibleCells
        let tableHeight: CGFloat = tblView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    
    func productsHistory() {
        OrdersRelatedParse.OrderHistoryList { (val) in
            DispatchQueue.main.async {
                if let val = val as? [orderhistorys] {
                    self.previousOrderHistory = val
                    self.tblView.reloadData()
                    
                }
                
            }
            
        }
    }
    
    func trackAction(_ sender: UIButton) {
        let orderId = previousOrderHistory[sender.tag].orderIdRef
        OrdersRelatedParse.orderTrackingList(id: orderId) { (arr) in
            DispatchQueue.main.async {
                
                let orderStatus = arr as? String
                
                
                //self.showStatus(message: orderStatus!)
            }
        }
    }
    
    func showStatus(message: String) {
        var status = ""
        if message == "1" {
            status = "Order Confirmed"
        }
        else if(message == "2") {
           status = "Order Dispatched"
        }
        else if (message == "3"){
            status = "Order on the way"
        }
        else{
            status = "Order delivered"
        }
        
        let alertController = UIAlertController(title: "Track Order", message: "Order Status: \(status)", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok",style: .default,handler: nil)
        alertController.addAction(okAction)
        present(alertController,animated: true,completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return previousOrderHistory.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistTableViewCell", for: indexPath) as? OrderHistTableViewCell
        let myObj = previousOrderHistory[indexPath.row]
        cell?.priceLabel.text = myObj.price
        cell?.proNameLabel.text = myObj.itemNames
        cell?.qntyLabel.text = myObj.quantity
//        var status = ""
//        if message == "1" {
//            status = "Order Confirmed"
//        }
//        else if(message == "2") {
//            status = "Order Dispatched"
//        }
//        else if (message == "3"){
//            status = "Order on the way"
//        }
//        else{
//            status = "Order delivered"
//        }
        cell?.trackOrderlbl.tag = indexPath.row
        cell?.trackOrderlbl.addTarget(self, action: #selector(trackAction), for: .touchUpInside)
    
        
        return cell!
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{
            previousOrderHistory.remove(at: indexPath.row)
            tblView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
