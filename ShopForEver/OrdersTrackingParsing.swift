//
//  OrdersTrackingParsing.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/16/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation

typealias QuantityCheckCompletionHandler = (Any) -> Void
typealias OrderHistoryComletionHandler = (Any) -> Void
typealias OrderTrackingListCompletionHandler = (Any)-> Void
class OrdersRelatedParse{
    static func noOfOrdersList(userOrdersObj: ordersPassingValue,completion: @escaping QuantityCheckCompletionHandler){
let x = userOrdersObj.finalValue.replacingOccurrences(of: "$", with: "")
      let y = x.replacingOccurrences(of: " ", with: "")
    let constantUrl = commonURLString + "orders.php?&item_id=%@&item_names=%@&item_quantity=%@&final_price=%@&mobile=%@"
    let strUrl = String(format: constantUrl, userOrdersObj.itemId,userOrdersObj.itemName,userOrdersObj.itemQuantity,y,userOrdersObj.mobile)
        var myUrl = URL(string: strUrl)
        print(URL(string: strUrl))
    var quantity: noOfOrders?
        print(strUrl)
        print(myUrl)
    
    URLSession.shared.dataTask(with: myUrl!) { (data, response, error) in
        
        if error == nil{
            do{
                
                
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String,Any> {
                    guard let arr = jsonResult["Order Confirmed"] as? [[String:Any]]else{
                        return
                    }
                    for item in arr{
                        let obj = item["OrderId"] as! Double 
                        print(obj)
                       quantity = noOfOrders(odrId: String(obj))
                        
                    }
                }
            }catch{
            }
        }
        completion(quantity!)
    }.resume()
}
    
    static func OrderHistoryList(completion: @escaping OrderHistoryComletionHandler) {
        let constantUrl = "\(commonURLString)order_history.php?&mobile=%@"
        print(constantUrl)
        //let strUrl = String(format: constantUrl, globalUserMobile!)
        let strUrl = String(format: constantUrl,globalUserMobile!)
        let url = URL(string: strUrl)
        var historyOfOrders =  [orderhistorys]()
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil{
                do{
                    
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String,Any> {
                        
                        guard let arr = jsonResult["Order History"] as? [[String: Any]] else{
                            return
                        }
                        
                        for item in arr {
                            let obj = orderhistorys(orderIdRef: item["OrderID"] as! String, itemNames: item["ItemName"] as! String, quantity: item["ItemQuantity"] as! String, price: item["FinalPrice"]as! String, statusOfOrder: item["FinalPrice"] as! String)
                            
                            print(obj)
                            historyOfOrders.append(obj)
                            print(historyOfOrders)
                        }
                        
                    }
                }catch{}
            }
            
            completion(historyOfOrders)
        }.resume()
        
    }
    
    static func orderTrackingList(id: String,completion: @escaping OrderTrackingListCompletionHandler) {
        let constantUrl = "\(commonURLString)/order_track.php?order_id=%@"
        
        let strUrl = String(format: constantUrl, id)
        let url = URL(string: strUrl)
        var orderTrackingObj = [orderTrackingValue]()
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                
                do{
                  
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String:Any]]{
                           print(jsonResult)
                        
                        for item in jsonResult {
                            let obj = orderTrackingValue(orderStatus: item["OrderStatus"] as! String)
                            print(obj)
                            orderTrackingObj.append(obj)
                        }
                    }
                    
                       // let stausOfOrder = jsonResult?["OrderStatus"] as! String
                 
                }catch{}
            }
            
            completion(orderTrackingObj)
        }.resume()
    }
    }
