//
//  CategeoryListTableViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/17/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import  SDWebImage
class CategeoryListTableViewController: UITableViewController {
    
    var strImg: URL?
    var refresh = UIRefreshControl()
    var resultsArray = [ProductCategeoryList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Categeories"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = 100
        refresh.isEnabled = true
        refresh.addTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
    getCategeories()
        
    }
    
    func refreshAction(_ sender: Any) {
        getCategeories()
    }
    
   
    func getCategeories() {
        OrdersParsng.categeoryList { (arr) in
            DispatchQueue.main.async {
                self.refresh.endRefreshing()
                self.resultsArray = (arr as? [ProductCategeoryList])!
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return resultsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategeoryViewCell", for: indexPath) as? CategeoryViewCell
        cell?.descrpLbl.text = resultsArray[indexPath.row].cagryDescp
        cell?.nameLbl.text = resultsArray[indexPath.row].cgryName
        cell?.idLabel.text = resultsArray[indexPath.row].idRef
       //cell?.itemsImage.sd_setImage(with: URL(string), placeholderImage: <#T##UIImage?#>, options: <#T##SDWebImageOptions#>, completed: <#T##SDExternalCompletionBlock?##SDExternalCompletionBlock?##(UIImage?, Error?, SDImageCacheType, URL?) -> Void#>)
        cell?.itemsImage.sd_setImage(with: URL(string: resultsArray[indexPath.row].cgryImage ), placeholderImage: UIImage(named: "noImage"))

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        if let controller = storyBoard.instantiateViewController(withIdentifier: "IndiItemsDisplayTableViewController") as? IndiItemsDisplayTableViewController{
            let obj = resultsArray[indexPath.row]
            print(obj)
            //obj.idRef
//            var result: ProductCategeoryList?
            
            
                
            controller.strValue = resultsArray[indexPath.row].idRef
           // print(controller)
                navigationController?.pushViewController(controller, animated: true)
            
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
