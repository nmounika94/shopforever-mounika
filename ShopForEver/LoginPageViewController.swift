//
//  LoginPageViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/14/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import RevealingSplashView
import TWMessageBarManager
import LGSideMenuController

let loginUrl = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/shop_login.php?mobile=123456&password=ansari"

class LoginPageViewController: UIViewController,UITextFieldDelegate {
    
    
    //var myText = [Forgotpassword]()
    
    @IBOutlet weak var phoneNum: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var lbltext: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login"
        
        //      let str = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/cust_category.php"
        //        let urls = URL(string: str)
        //        OrdersParsng.categeoryList { (array) in
        //
        //        }
        //
        
        //        let str = "https://rjtmobile.com/ansari/shopingcart/ios_ssl/cust_sub_category.php?Id=107"
        //        let urls = URL(string: str)
        //        OrdersParsng.subCategeoryList(url: urls!, passingCtryObj:
        //        proSubCtryPassingList(idValue: "109"))
        //        OrdersParsng.subCategeoryList(url: urls!, passingCtryObj: proSubCtryPassingList(idValue: "107")) { (arr) in
        //
        //        }
        
        //
       
        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "shoppy")!, iconInitialSize: CGSize(width: 100, height: 100), backgroundColor: UIColor.blue)
        
        revealingSplashView.useCustomIconColor = true
        revealingSplashView.iconColor = UIColor.brown
        revealingSplashView.animationType = SplashAnimationType.woobleAndZoomOut
        self.view.addSubview(revealingSplashView)
        revealingSplashView.startAnimation(){
            print("Completed")
        }
        
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var lgnBtn: UIButton!
    
    @IBAction func loginBtn(_ sender: UIButton) {
        
        let bounds = self.lgnBtn.bounds
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: [], animations: {
            self.lgnBtn.bounds = CGRect(x: bounds.origin.x - 20, y: bounds.origin.y, width: bounds.size.width + 60, height: bounds.size.height)
            self.lgnBtn.isEnabled = false
        }, completion: nil)
        
        parsingData.loginExixstingUser(userLoginObj:userLogin(userMobile: phoneNum.text!, password: password.text!, message: "")) { (val) in
            
            if val is UserSignedIn {
                
                DispatchQueue.main.async {
                    
                    let userLoggedInObj = val as? UserSignedIn
                    print(userLoggedInObj ?? "Error")
                    
                    globalUserName = userLoggedInObj?.userName
                    globalUserMobile = userLoggedInObj?.userMobile
                    globalUserEmail = userLoggedInObj?.userEmail
                    
                   TWMessageBarManager.sharedInstance().showMessage(withTitle: "Login successfull", description: "You have successfully logged in", type: .success)
                    if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CategeoryListTableViewController") as? CategeoryListTableViewController{
                        
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                }
            }
            
            else {
                
//                let x = val as? errorModel
//                print(x ?? "No luck")
                TWMessageBarManager.sharedInstance().showMessage(withTitle: "Please enter the correct information", description: "Error Logging In", type: .success)
               // self.view.dodo.error((x?.message)!)
            }
            
            
            
            
//            var myData = val as? userLogin
//            let storyBoad = UIStoryboard(name: "Main", bundle: nil)
//            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "CategeoryListTableViewController") as? CategeoryListTableViewController{
//                self.navigationController?.pushViewController(controller, animated: true)
//            }
            
        
    }
    }
    
    @IBAction func forgotPswd(_ sender: UIButton) {
        parsingData.ForgotPassword(userForgotPswdObj: Forgotpassword(usermobileNo: phoneNum.text!, userpswd: password.text!)) { (value) in
            
           
            
            
            DispatchQueue.main.async {
                print(value)
                 let myText = value as? Forgotpassword
                globalForgotPassword = myText?.userpswd
                self.lbltext.text = myText?.userpswd
                print(myText?.userpswd)
                
                //                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //
                //
                //                let controller = storyBoard.instantiateViewController(withIdentifier: "ForgotPswdViewController") as? ForgotPswdViewController
                //                //controller.str = self.lbltext.text!
                //                self.navigationController?.pushViewController(controller!, animated: true)
            }
        }
        
        
        
    }
    @IBAction func RegisterBtn(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        
        let controller = storyBoard.instantiateViewController(withIdentifier: "RegisterPageViewController")
        self.present(controller, animated: true, completion: nil)
        
        
        
    }
    @IBAction func resetPasswrd(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "PasswordViewController") {
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  }
