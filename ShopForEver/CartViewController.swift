//
//  CartViewController.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/19/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import TWMessageBarManager
import InteractiveSideMenu

class CartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,PayPalPaymentDelegate,SideMenuItemContent {
    
    var objorders: ordersPassingValue?
    var environment:String = PayPalEnvironmentNoNetwork {
    willSet(newEnvironment) {
      if (newEnvironment != environment) {
        PayPalMobile.preconnect(withEnvironment: newEnvironment)
      }
    }
  }
     var payPalConfig = PayPalConfiguration()
    @IBOutlet weak var tblView: UITableView!
    
    var count = 0
    var itemsInstock = 0
    var value = 0
    var indexPathRow = 0
    var resultsDisplay = [eachProductslist]()
    var price: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    

  payPalConfig.acceptCreditCards = false
    payPalConfig.merchantName = "Awesome Shirts, Inc."
    payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
    payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
         payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        payPalConfig.payPalShippingAddressOption = .both;
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    PayPalMobile.preconnect(withEnvironment: environment)
  }
//    @IBAction func sidemenuBtn(_ sender: UIButton) {
//    showSideMenu()
//}
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return globalCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cartObj = globalCart[indexPath.row]
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as? CartTableViewCell
        let priceString = cartObj.prize
        price = Int(priceString)
        priceString.replacingOccurrences(of: ",", with: "")
        let priceInDollars = Int(priceString)!/67
        cell?.priceLabel.text = " \(Int(priceInDollars))$"
        cell?.qntyLabel.text = cartObj.quantity
        cell?.nameLabel.text = cartObj.prodName
        cell?.descpLabel.text = cartObj.descrip
        cell?.proId.text = cartObj.idRefere

        cell?.chkOutBtn.tag = indexPath.row
        cell?.chkOutBtn.addTarget(self, action: #selector(checkOutBtnAction), for: .touchUpInside)
        cell?.addBtn.tag = indexPath.row
        cell?.addBtn.addTarget(self, action: #selector(addBtn), for: .touchUpInside)
        cell?.subBtn.tag = indexPath.row
        cell?.subBtn.addTarget(self, action: #selector(subBtn), for: .touchUpInside)
        let imgUrl = cartObj.img
        let url = URL(string: imgUrl!)
        
        
        cell?.proImg.sd_setImage(with: url, placeholderImage: UIImage(named:"noImage"), options: [], completed: nil)
        return cell!
    }
    
    func productQuantityAction(sender: UIButton) {
        
        var indexPath = IndexPath(row: sender.tag, section: 0)
        indexPathRow = sender.tag
        let cartObj = globalCart[indexPath.row]
        itemsInstock = Int(cartObj.quantity!)!
        
        
        
    }
    
    func addBtn(sender: AnyObject) -> Int{
                    let button:UIButton = sender as! UIButton
        
                //defines what indexPath is and what it is used to define a cell.
                let indexPath = NSIndexPath(item: sender.tag, section: 0)
                let cell = tblView.cellForRow(at: indexPath as IndexPath) as!CartTableViewCell!
        
                count = 1 + count
                print(count)
        
                let result = globalCart[indexPath.row]
                    var quanti = result.quantity
                    if count < Int(quanti!)! {
                      count += 1
                        var priceResult = result.prize
                       price = Int(priceResult)! * count
                        print(price)
                        let priceInDollas = Double(price!)/67.0
                        cell?.priceLabel.text = " \(Int(priceInDollas))"
        
                    }
                    else {
        
                        button.isEnabled = false
                    }
        
            cell?.qntyDisplay.text = "\(count)"
        print(cell?.qntyDisplay.text)
              // cell?.qntyLabel.text = "\(count)"
                return count
                    tblView.reloadData()
        
            }
        
            func subBtn(sender: UIButton) -> Int {
        
                let indexPath = NSIndexPath(item: sender.tag, section: 0)
                let cell = tblView.cellForRow(at: indexPath as IndexPath) as! CartTableViewCell!
        
                if count == 0 {
                    print(" Count Zero")
                }
                else{
                  count = count - 1
                    let result = globalCart[indexPath.row]
                    var priceResult = result.prize
                    price = Int(priceResult)! * count
                    print(price)
                    let priceInDollas = Double(price!)/67.0
                    cell?.priceLabel.text = " \(Int(priceInDollas))"
                    
                }
                cell?.qntyDisplay.text = "\(count)"
                //cell?.qntyLabel.text = "\(count)"
                print(count)
                tblView.reloadData()
                return count
               
            }

    func checkOutBtnAction(_ sender: UIButton) {

    let valueforItems = globalCart[sender.tag]
        value = sender.tag
let item1 = PayPalItem(name: "Old jeans with holes", withQuantity: 2, withPrice: NSDecimalNumber(string: "84.99"), withCurrency: "USD", withSku: "Hip-0037")
    let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
    let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
    
    let items = [item1, item2, item3]
    let subtotal = PayPalItem.totalPrice(forItems: items)
    
    // Optional: include payment details
    let shipping = NSDecimalNumber(string: "5.99")
    let tax = NSDecimalNumber(string: "2.50")
    let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
    
    let total = subtotal.adding(shipping).adding(tax)
    
    let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Hipster Clothing", intent: .sale)
    
    payment.items = items
    payment.paymentDetails = paymentDetails
    
    if (payment.processable) {
      let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
      present(paymentViewController!, animated: true, completion: nil)
    }
    else {
      // This particular payment will always be processable. If, for
      // example, the amount was negative or the shortDescription was
      // empty, this payment wouldn't be processable, and you'd want
      // to handle that here.
      print("Payment not processalbe: \(payment)")
    }
    
  }
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
    print("PayPal Payment Cancelled")
    //resultText = ""
   // successView.isHidden = true
    paymentViewController.dismiss(animated: true, completion: nil)

}
   
  
  func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
    print("PayPal Payment Success !")
    paymentViewController.dismiss(animated: true, completion: { () -> Void in
      // send completed confirmaion to your server
      print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
      
     
        let ipath = IndexPath(row: self.value, section: 0)
        let cell = self.tblView.cellForRow(at: ipath) as? CartTableViewCell
let quant = cell?.qntyDisplay.text
let price = cell?.priceLabel.text
        let id = cell?.proId.text
        let proName = cell?.nameLabel.text
    
//print(quant)
//print(price)
//print(id)
//print(proName)
        //myObj = global[sender.tag]
        
       self.objorders = ordersPassingValue(itemId: id!, itemName: proName!, itemQuantity: quant!, finalValue: price!, mobile: globalUserMobile!)
//print(self.objorders?.itemQuantity)
//print(self.objorders?.itemId)
//print(self.objorders?.mobile)
//        print(self.objorders?.finalValue)
//print(self.objorders?.itemName)

        OrdersRelatedParse.noOfOrdersList(userOrdersObj: self.objorders!, completion: { (val) in
            print(val)
            DispatchQueue.main.async {
        TWMessageBarManager.sharedInstance().showMessage(withTitle: "Order Confirmed", description: "Your order has been confirmed", type: .success)
        
                let alertController = UIAlertController(title: "Alert", message: "OrderConfirmed", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    self.present(alertController,animated: true,completion: nil)
                })
                
}
})
    })
  }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
