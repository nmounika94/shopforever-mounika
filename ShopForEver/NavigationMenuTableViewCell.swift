//
//  NavigationMenuTableViewCell.swift
//  Cyclops
//
//  Created by Naveen Magatala on 9/18/17.
//  Copyright © 2017 Naveen Magatala. All rights reserved.
//

import UIKit

class NavigationMenuTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var navigationImage: UIImageView!
    @IBOutlet weak var navigationName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
