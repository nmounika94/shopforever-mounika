//
//  ItemsDisplayTableViewCell.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/17/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class ItemsDisplayTableViewCell: UITableViewCell {
    @IBOutlet weak var itemId: UILabel!
    @IBOutlet weak var itemDescp: UILabel!

    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var imgItems: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
