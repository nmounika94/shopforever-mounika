//
//  ParsingData.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/15/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
// 9490057172
//mounika

import Foundation

typealias registerCompletionHandler = (Any) -> Void
typealias loginCompletionHandler = (Any) -> Void
typealias resetCompletionHandler = (Any) -> Void
typealias forgotPswdCompletionHandler = (Any) -> Void

class parsingData{
    
    static func SignUpNewUse(userSignUpObj: userSignedUpModel,completion: @escaping registerCompletionHandler) {
        
        var text: String = ""
        let constanturl = "\(commonURLString)/shop_reg.php?name=%@&email=%@&mobile=%@&password=%@"
        let strUrl = String(format: constanturl, userSignUpObj.userName,userSignUpObj.userEmail,userSignUpObj.userMobile,userSignUpObj.password)
        print(strUrl)
        let url = URL(string: strUrl)
        print(url)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                  text = String(data: data!,encoding: .utf8)!
                
                print(text ?? "No text")
            }
            else {
                print(error?.localizedDescription ?? " no description")
            }
            completion(text)
            
        
        }.resume()

    }
    
    static func loginExixstingUser(userLoginObj: userLogin, completion: @escaping loginCompletionHandler){
        
        let constantUrl = "\(commonURLString)/shop_login.php?mobile=%@&password=%@"
        let strUrl = String(format: constantUrl, userLoginObj.userMobile,userLoginObj.password)
        let url = URL(string: strUrl)
        var userloggedInObj: UserSignedIn?
        var userErrorObj: userLogin?
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            
            if error == nil{
                do{
                    let result = try JSONSerialization.jsonObject(with: data!, options: [])
                    if result is [String: Any] {
                        let object = result as? [String: Any]
                        let messages = object? ["msg"] as? [String]
                        for message in messages! {
                            userErrorObj = userLogin(userMobile: "", password: "", message: message)
                        }
                        if let res = userErrorObj{
                            
                            completion(res)
                        }
                        
                    }
                    else if result is [[String: Any]]{
                        let objects = result as? [[String: Any]]
                        for object in objects!{
                            let msg = object["msg"] as! String
                            let name = object["UserName"] as! String
                            let email = object["UserEmail"] as! String
                            let mobile = object["UserMobile"] as! String
                            
                            print(msg)
                            print(name)
                            print(email)
                            print(mobile)
                            
                            userloggedInObj = (UserSignedIn(message: msg, userName: name, userEmail: email, userMobile: mobile))
                           // userloggedInObj.append(UserSignedIn(message: msg, userName: name, userEmail: email, userMobile: mobile))
                        }
                    }
                    
                    if let res = userloggedInObj{
                        completion(res)
                    }
                  print(userLoginObj )
                    print(userErrorObj ?? "No Luck")
                    print(userloggedInObj ?? "My Details")
                    
                }catch{}
                
                            }
            else {
                print(error?.localizedDescription ?? "No description")
            }
            
        }.resume()
    }
    
    static func resetPswd(resetpswdObj: ResetpasswordModel,completion: @escaping resetCompletionHandler) {
    let rawStrUrl = "\(commonURLString)shop_reset_pass.php?&mobile=%@&password=%@&newpassword=%@"
        
        let strUrl = String(format: rawStrUrl, resetpswdObj.userMobileNo,resetpswdObj.userpaswd,resetpswdObj.newpswd)
        
        let url = URL(string: strUrl)
        var userresetPswdObj:ResetpasswordModel?
        var pswderrorObj: pswderrorModel?
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error == nil {
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String,[Any]>
//                    if result is [String: Any] {
//                        let object = result as? [String : Any]
//                        let messages = object? ["msg"] as? [String]
//                        
//                        let text = String(data:data!,encoding: .utf8)
//                       print(userresetPswdObj ?? "No luck")
//                        for message in messages!{
//                            pswderrorObj = pswderrorModel(message: message)
//                            print(pswderrorObj)
//                        }
//                        }

                    
                            let msg = result?["msg"]?[0] as! String
//                            let mobileNo = result["mobile"] as! String
//                            let newpswd = result["password"] as! String
//                            let cnfrmPswd = object["newpassword"] as! String
//                            userresetPswdObj = ResetpasswordModel(userMobileNo: mobileNo, userpaswd: cnfrmPswd, newpswd: cnfrmPswd)
//                            print(userresetPswdObj)
                    print(msg)
                            //start here
                        
                    
                }catch{}
                }
            
            completion(msg.self)
            }.resume()
        }
    
    static func ForgotPassword(userForgotPswdObj: Forgotpassword,completion: @escaping forgotPswdCompletionHandler){
        
        
       
        let constantUrl = "\(commonURLString)/shop_fogot_pass.php?mobile=%@&password=%@"
        let strUrl = String(format: constantUrl, userForgotPswdObj.usermobileNo,userForgotPswdObj.userpswd)
        let url = URL(string: strUrl)
        
        
        var errorObj: ForgotPswdErrorModel?
        var passingInfo = userForgotPswdObj
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error == nil {
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: [])
                    if result is Dictionary<String,[String]> {
                        let object = result as? Dictionary<String,[String]>
                       // let object = result as? [[String: Any]]
                         let messages = object? ["msg"] as? [String]
                        for message in messages!{
                            errorObj = ForgotPswdErrorModel(message: message)
                        }
                    }
                    
                    else if result is Array<[String: String]>{
                        //let objects = result as? [[String: Any]]
                        let objects = result as? Array<[String: String]>
                        for object in objects!{
                            
                            let msg = object["msg"] as! String
                            let mobile = object["UserMobile"] as! String
                            let pswd = object["UserPassword"] as! String
                            
                            
//                           let obj = Forgotpassword(usermobileNo: object["UserMobile"]as! String, userpswd: object["UserPassword"]as! String)
                            
                           passingInfo = Forgotpassword(usermobileNo: mobile, userpswd: pswd)
                            
                            print(passingInfo)
                        }
                    }
                    
                }catch{}
            }
            completion(passingInfo)
        }.resume()
    }
    
    }


