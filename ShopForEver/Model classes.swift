//
//  Model classes.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/15/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation

struct userLogin {
    var userMobile: String
    var password: String
    var message: String
}
struct UserSignedIn {
    var message: String
    var userName: String
    var userEmail: String
    var userMobile: String
}
struct errorModel {
    var message: String
}
struct  userSignedUpModel {
    var userName: String
    var userEmail: String
    var userMobile: String
    var password: String
}
struct ResetpasswordModel {
    var userMobileNo : String
    var userpaswd : String
    var newpswd : String
    
}
struct pswderrorModel {
    var message: String
}
struct  Forgotpassword {
    var usermobileNo: String
    var userpswd: String
    
}
struct ForgotPswdErrorModel {
    var message: String
}
