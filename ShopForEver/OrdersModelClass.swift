//
//  OrdersModelClass.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/16/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation

struct ProductCategeoryList{
    
    var idRef: String
    var cgryName: String
    var cagryDescp: String
    var cgryImage: String
    
}

struct productSubCtryList{
    
    var idRef: String
    var subCatrgyName: String
    var subCatryDescrp: String
    var CtrgyImage: String
}

struct proSubCtryPassingList {
    var idValue: String
}

struct eachProductslist {
    var idRefere: String?
    var prodName: String?
    var quantity: String?
    var prize: String
    var descrip: String?
    var img: String?
}

struct eachpdtPassingList {
    var idValue: String?
}


