//
//  OrderHistTableViewCell.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/20/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class OrderHistTableViewCell: UITableViewCell {

    @IBOutlet weak var trackOrderlbl: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var qntyLabel: UILabel!
    @IBOutlet weak var proNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
