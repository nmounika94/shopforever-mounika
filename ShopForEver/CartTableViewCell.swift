//
//  CartTableViewCell.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/19/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    @IBOutlet weak var proImg: UIImageView!
    @IBOutlet weak var subBtn: UIButton!

    @IBOutlet weak var chkOutBtn: UIButton!
    @IBOutlet weak var qntyDisplay: UILabel!
    @IBOutlet weak var qntyLabel: UILabel!
   
    @IBOutlet weak var descpLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var proId: UILabel!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
