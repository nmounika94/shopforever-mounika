//
//  OrdersRelatedParse.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/16/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation

typealias OrdersListCompletionHandler = (Any) -> Void
typealias subCatgryCompletionHandler = (Any) -> Void
typealias eachProductCompletionHandler = (Any) -> Void

class OrdersParsng {
    static func categeoryList(completion: @escaping OrdersListCompletionHandler){
       var  orderListObj = [ProductCategeoryList]()
        
        let constanturl = "\(commonURLString)cust_category.php"
        let strUrl  = URL(string: constanturl)
        
        URLSession.shared.dataTask(with: strUrl!) { (data, response, error) in
            
            if error == nil{
                do{
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String,Any>{
                       guard let arr = jsonResult["Category"] as? [[String: Any]] else{
                           return
                        }
                        
                        for items in arr {
                            
                            let obj = ProductCategeoryList(idRef: items["Id"] as! String, cgryName:items["CatagoryName"] as! String, cagryDescp: items["CatagoryDiscription"]as! String, cgryImage: items["CatagoryImage"]as! String)
                            orderListObj.append(obj)
                            print(obj)
                            print(orderListObj)
                        }
                      // print(arr)
                       // print(jsonResult)
                        
                        
                    }
                }catch{
                    
                }
            }
         
            completion(orderListObj)
            
        }.resume()
    }
    
    static func subCategeoryList(id : String,completion: @escaping subCatgryCompletionHandler) {
        
        //let passingCtryObj: proSubCtryPassingList
       
        let strUrl = String(format: constantUrl,id)
        let url = URL(string:strUrl)
        var subCtrgyObj = [productSubCtryList]()
        //var subctryObj
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error == nil{
                
                do{
                    if let result = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String,Any> {
                        
                        guard let arr = result["SubCategory"] as? [[String: Any]] else{
                            return
                            
                        }
                        print(arr)
                        print(strUrl)
                        print(url)
                        for items in arr {
                            
                            let obj = productSubCtryList(idRef: items["Id"] as! String, subCatrgyName: items["SubCatagoryName"] as! String, subCatryDescrp: items["SubCatagoryDiscription"] as! String, CtrgyImage: items["CatagoryImage"] as! String)
                            subCtrgyObj.append(obj)
                        }
                        //print(arr)
                        //print(result)
                       // print(url)
                        //print(strUrl)
                        print(subCtrgyObj)
                    }
                    
                    
                }catch{}
            }
            
            completion(subCtrgyObj)
        }.resume()
    }
    
    static func eachProductValue(id: String,completion: @escaping eachProductCompletionHandler) {
        
        let strUrl = String(format: constantUrl1, id)
        let url = URL(string: strUrl)
        var eachOrderobjValues = [eachProductslist]()
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error == nil{
                do{
                    if let result = try JSONSerialization.jsonObject(with: data!, options: []) as? Dictionary<String,Any>{
                        
                        guard let arr = result["Product"] as? [[String: Any]] else{
                            return
                        }
                        
                        for items in arr {
                            let obj = eachProductslist(idRefere: items["Id"] as? String, prodName: items["ProductName"] as? String, quantity: items["Quantity"] as? String, prize: items["Prize"] as! String, descrip: items["Discription"] as? String ?? "", img: items["Image"] as? String ?? "noImage")
                            
                            
                           eachOrderobjValues.append(obj)
                           print(obj)
                            
                        }
                        //print(arr)
                        //print(result)
                    }
                }catch{}
            }
            completion(eachOrderobjValues)
        }.resume()
        
    }
}
