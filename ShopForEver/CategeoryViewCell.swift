//
//  CategeoryViewCell.swift
//  ShopForEver
//
//  Created by Mounika Nerella on 9/17/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class CategeoryViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descrpLbl: UILabel!
    @IBOutlet weak var itemsImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
